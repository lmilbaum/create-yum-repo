import logging
import subprocess
from os import PathLike
from pathlib import Path
from typing import Optional


def create_repo(repodir: Path):
    """Create a YUM repo at `repodir` using createrepo_c."""
    _run_cmd(cmd=["createrepo_c", str(repodir)])


def create_index_html(topdir: Path):
    """Create an index file for the tree starting with `topdir`."""
    index_file = topdir / "index.html"
    _run_cmd(cmd=["tree", "-H", ".", str(topdir), "-o", str(index_file)])


def dnf_makecache(
    *repos: str,
    conf_file: Optional[PathLike] = None,
    verbose: bool = True,
):
    options = []
    if verbose:
        options.append("-v")

    if conf_file is not None:
        options.extend(["-c", str(conf_file)])

    if repos:
        options.extend([f"--repo={repo}" for repo in repos])

    _run_cmd(cmd=["dnf", *options, "makecache"])


def _run_cmd(cmd: list[str]):
    logging.info("Running %s", " ".join(cmd))
    subprocess.run(
        cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True
    )
