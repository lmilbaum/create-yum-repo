#!/usr/bin/env python3
import asyncio
import logging
import logging.config
import tempfile
from argparse import ArgumentParser
from pathlib import Path

from jinja2 import Template

from .external_commands import dnf_makecache


async def verify_repo(baseurl: str, arch: str):
    """Call dnf makecache to test the created repos are valid"""
    logging.info("Testing dnf repo at %s for %s", baseurl, arch)

    # Configure and call dnf using a custom dnf.conf to avoid contaminating
    # the host system
    templates = Path(__file__).parent / "templates"
    dnfconf_template_file = templates / "dnf.conf.j2"
    repofile_template_file = templates / "yumrepo.j2"

    with tempfile.TemporaryDirectory() as tmpdirname:
        tmpdir = Path(tmpdirname)

        (tmpdir / "cache").mkdir(parents=True)
        (tmpdir / "logs").mkdir(parents=True)
        (tmpdir / "persist").mkdir(parents=True)
        (tmpdir / "repos").mkdir(parents=True)

        dnfconf_file = tmpdir / "dnf.conf"
        repofile = tmpdir / "repos" / "automotive.repo"

        dnfconf = Template(dnfconf_template_file.read_text()).render(
            tmpdir=tmpdirname
        )
        dnfconf_file.write_text(dnfconf)

        repoconf = Template(repofile_template_file.read_text()).render(
            arch=arch, baseurl=baseurl
        )
        repofile.write_text(repoconf)
        logging.info(repofile)

        dnf_makecache("auto", conf_file=tmpdir / "dnf.conf", verbose=True)


def arg_parser():
    parser = ArgumentParser(
        description=(
            "Verifies YUM repo metadata are well-formed by calling "
            "dnf makecache."
        )
    )
    parser.add_argument("repo", help="The URL of the YUM repo to be verified.")
    return parser


async def main():
    logging.info(":: test-yum-repo started ::")

    parser = arg_parser()
    args = parser.parse_args()

    supported_arches = ["aarch64", "x86_64"]

    logging.info("Testing repos at: %s", args.repo)

    tasks = [
        asyncio.create_task(verify_repo(args.repo, arch))
        for arch in supported_arches
    ]
    await asyncio.gather(*tasks)
    logging.info("Success!")


def run():
    asyncio.get_event_loop().run_until_complete(main())


if __name__ == "__main__":
    run()
