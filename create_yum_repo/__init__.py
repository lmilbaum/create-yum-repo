import logging
import sys

logging.basicConfig(
    stream=sys.stdout,
    format=(
        "%(asctime)s [%(levelname)-8s] %(funcName)s(%(threadName)s) "
        "%(message)s"
    ),
    datefmt="%Y-%m-%d %H:%M:%S %z",
    style="%",
    level=logging.INFO,
)
