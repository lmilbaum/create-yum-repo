#!/usr/bin/env python3
import asyncio
import logging
import logging.config
import time
from argparse import ArgumentParser
from datetime import timedelta
from itertools import product
from pathlib import Path
from typing import Collection, Mapping
from urllib.parse import urlparse

from .download import (
    CENTOS_REPOS,
    DEFAULT_KOJI_INSTANCES,
    DEFAULT_YUM_REPOS,
    SUPPORTED_ARCHES,
    download_packages,
)
from .external_commands import create_index_html, create_repo
from .nvra import build_nvra_lookup

ALLOWED_DISTRIBUTIONS = ["cs9", "rhel9"]


def argument_parser(args=None):
    parser = ArgumentParser(
        description="Create a YUM repo from constituent packages."
    )
    parser.add_argument(
        "base",
        choices=ALLOWED_DISTRIBUTIONS,
        help=(
            "The base distribution for the packages in the resulting YUM "
            "repository. "
        ),
    )
    parser.add_argument(
        "-l,--lockfile",
        dest="lockfile",
        type=Path,
        required=True,
        help="Path to the lockfile.",
    )
    parser.add_argument(
        "-o,--output-dir",
        dest="output_dir",
        type=Path,
        default=Path("/var/lib/repos"),
        help=(
            "The intermediate directory where RPMs will be downloaded and "
            "the YUM repo created."
        ),
    )
    parser.add_argument(
        "-r,--repo-url",
        dest="repo_urls",
        action="extend",
        nargs="+",
        help="A list of repo URLs in descending priority order.",
    )
    parser.add_argument(
        "-k,--koji",
        dest="koji_instances",
        action="extend",
        nargs="+",
        help="A list of Koji instance URLs.",
    )
    return parser


def _repository_priorities(repo_urls: Collection[str]) -> Mapping[str, int]:
    # repo_domains is built from the raw list of repo URLs
    # based on the assumption that they are in priority order at this point.
    # This could be done after expansion, as duplicates don't really
    # affect the priority mapping.
    # However, this cannot be done after de-duplication since the conversion
    # to a Set for deduplication undoes the priority ordering.
    repo_domains = map(lambda u: urlparse(u).netloc, repo_urls)

    # mapping from repo_domain (e.g. mirrors.stream.centos.org)
    # to its priority value
    return dict(
        (domain, priority) for priority, domain in enumerate(repo_domains)
    )


def _expand_repository_urls(repo_urls: Collection[str]) -> Collection[str]:
    # expand the raw repo URLs by substituting in the supported architectures
    # and RPM repo names e.g. BaseOS, AppStream, etc
    repo_mirrors = []
    for url in repo_urls:
        for repo, arch in product(CENTOS_REPOS, SUPPORTED_ARCHES):
            # any extra format args are ignored, so if a URL doesn't need
            # centos_repo or arch nothing will raise an error
            repo_mirrors.append(url.format(centos_repo=repo, arch=arch))

    # dedupe after expanding all the repo mirror URLs
    return set(repo_mirrors)


async def run(args=None):
    parser = argument_parser()
    args = parser.parse_args(args=args)

    repo_urls = args.repo_urls or DEFAULT_YUM_REPOS
    koji_mirrors = args.koji_instances or DEFAULT_KOJI_INSTANCES

    logging.info("Create Yum Repo Started")

    start = time.perf_counter()

    # mapping from repo_domain (e.g. mirrors.stream.centos.org)
    # to its priority value
    repo_priorities = _repository_priorities(repo_urls)
    repo_mirrors = _expand_repository_urls(repo_urls)
    logging.info("Expanded %d unique repo URLs.", len(repo_mirrors))

    nvras = await build_nvra_lookup(repo_mirrors, repo_priorities)

    end = time.perf_counter()
    runtime = timedelta(seconds=end - start)

    logging.info(
        "NVRA lookup table complete. Indexed %d packages in %s",
        len(nvras),
        runtime,
    )
    logging.info(
        "Generating repo from %s in %s...", args.lockfile, args.output_dir
    )

    await download_packages(
        args.base,
        args.lockfile,
        nvras,
        koji_mirrors,
        args.output_dir,
    )

    logging.info("All package downloads complete.")

    # Generate yum repo index for downloaded packages
    logging.info("Creating YUM repo with createrepo_c...")
    for arch in SUPPORTED_ARCHES:
        create_repo(args.output_dir / args.base / arch / "os")

    # Generate index.html file for {repo_dir}/version
    logging.info("Generating index.html file...")
    create_index_html(args.output_dir)

    end = time.perf_counter()
    runtime = timedelta(seconds=end - start)
    logging.info("Create Yum Repo Finished (%s)", runtime)


def main(args=None):  # pragma: no cover
    asyncio.get_event_loop().run_until_complete(run(args))


if __name__ == "__main__":
    main()
