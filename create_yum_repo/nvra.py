import asyncio
import gzip
import logging
from asyncio import PriorityQueue
from collections import defaultdict
from typing import Collection, Mapping, MutableMapping
from urllib.parse import urljoin, urlparse
from xml.etree.ElementTree import Element, fromstring

import httpx

from .download import async_client
from .rpm_source import RpmSource

REPOMD_XML_NAMESPACES = {"": "http://linux.duke.edu/metadata/repo"}
PRIMARY_XML_NAMESPACES = {"": "http://linux.duke.edu/metadata/common"}


ARBITRARILY_LOW_PRIORITY = 999


async def build_nvra_lookup(
    repos: Collection[str], priority_mapping: Mapping[str, int]
) -> Mapping[str, PriorityQueue]:
    """Build a mapping from package NVRA to its download URLs for every package
    in each repo.

    Notes:
        The order in which repo URLs are passed to the main entrypoint imposes
        a natural priority order. In other words, if repo_url_a is passed
        before repo_url_b it means that if a package is available from both
        repo_url_a and repo_url_b then we should prefer to download it from
        repo_url_a.

        In order to preserve this priority ordering and also parallelize
        repository indexing, each NVRA is mapped to a PriorityQueue of
        RpmSource objects rather than a single URI. RpmSource objects
        encapsulate an RPM's download URI and the priority given to that URI.

        The priority is determined by looking up a URI's "netloc" property
        (from urlparse) in `priority_mapping` (using an arbitrarily high value
        - indicating a low priority if not present).

        Later the highest priority RpmSource is popped off of a package's
        queue, and the URI associated with that object is tried for download.

        Why are low numbers used to indicate high priority? The heap
        implementation in the standard library is a min-heap by default,
        meaning the values in the heap are ordered based on a `<` relationship,
        and the top of the heap is always the minimum value.

    Parameters:
        repos: A collection of repo URLs to index.
        priority_mapping: A mapping from repository domain to its priority
        value.This is used to ensure priority order for downloading packages is
        not lost while indexing asynchronously.

    Returns:
        The NVRA -> PriorityQueue mapping.
    """
    nvras = defaultdict(PriorityQueue)
    client = async_client()
    tasks = [
        asyncio.create_task(_index_repo(nvras, repo, priority_mapping, client))
        for repo in repos
    ]
    try:
        await asyncio.gather(*tasks)
        return nvras
    finally:
        await client.aclose()


async def _index_repo(
    nvras: MutableMapping[str, PriorityQueue],
    repo: str,
    priority_mapping: Mapping[str, int],
    client: httpx.AsyncClient,
):
    domain = urlparse(repo).netloc
    priority = priority_mapping.get(domain, ARBITRARILY_LOW_PRIORITY)

    # Avoid pulling lots of deps by just parsing the xml directly
    # Find primary.xml via repomd.xml
    repomd_url = urljoin(repo, "repodata/repomd.xml")
    repomd = await _fetch_repomd_xml(repomd_url, client)
    primary_xml_path = _primary_xml_path_from_repomd(repomd)
    logging.info("Primary xml at path %s", primary_xml_path)

    full_primary_xml_url = urljoin(repo, primary_xml_path)
    logging.info("Handling primary XML from %s", full_primary_xml_url)

    primary = await _fetch_primary_xml(full_primary_xml_url, client)
    logging.info("Resolved %d packages from %s", len(primary), repo)

    # Iterate through the package elements
    # If there are multiple versions of a package, they're luckily sorted
    # from oldest to newest so the final nevra is the newest
    for package in primary.iterfind(
        "package", namespaces=PRIMARY_XML_NAMESPACES
    ):
        name = package.find("name", namespaces=PRIMARY_XML_NAMESPACES)
        version = package.find("version", namespaces=PRIMARY_XML_NAMESPACES)
        arch = package.find("arch", namespaces=PRIMARY_XML_NAMESPACES)
        location = package.find("location", namespaces=PRIMARY_XML_NAMESPACES)

        n = name.text
        v = version.attrib["ver"]
        r = version.attrib["rel"]
        a = arch.text

        href = location.attrib["href"]

        nvra = f"{n}-{v}-{r}-{a}"
        url = urljoin(repo, href)
        rpm_source = RpmSource(url, priority)
        await nvras[nvra].put(rpm_source)


async def _fetch_repomd_xml(url: str, client: httpx.AsyncClient) -> Element:
    logging.info("Fetching repomd: %s", url)
    response = await client.get(url)
    response.raise_for_status()
    root = fromstring(response.content)
    await response.aclose()
    return root


def _primary_xml_path_from_repomd(repomd: Element) -> str:
    primary_tag = repomd.find(
        "data[@type='primary']/location", namespaces=REPOMD_XML_NAMESPACES
    )
    return primary_tag.attrib["href"]


async def _fetch_primary_xml(url: str, client: httpx.AsyncClient) -> Element:
    logging.info("Fetching primary XML: %s", url)
    try:
        response = await client.get(url)
        response.raise_for_status()
    except httpx.ReadTimeout as e:
        logging.error(str(e))
        raise

    primary_xml = gzip.decompress(response.content)
    await response.aclose()
    return fromstring(primary_xml)
