import asyncio
import json
import logging
import os
import shutil
from asyncio import PriorityQueue
from pathlib import Path
from typing import Any, Collection, Mapping, MutableMapping, Optional
from urllib.parse import urljoin

import httpx
import koji
from aiofile import AIOFile

CENTOS_REPOS = ["BaseOS", "AppStream", "CRB"]
SUPPORTED_ARCHES = ["aarch64", "x86_64"]
DEFAULT_YUM_REPOS = [
    "http://mirror.stream.centos.org/9-stream/{centos_repo}/{arch}/os/",
    "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/{centos_repo}/{arch}/os/",  # noqa E501
    "https://buildlogs.centos.org/centos/9-stream/automotive/{arch}/packages-main/",  # noqa E501
    "https://buildlogs.centos.org/centos/9-stream/autosd/{arch}/packages-main/",  # noqa E501
]
DEFAULT_KOJI_INSTANCES = [
    "https://kojihub.stream.centos.org/kojihub/|https://kojihub.stream.centos.org/kojifiles/packages/",  # noqa E501
    "https://cbs.centos.org/kojihub/|https://cbs.centos.org/kojifiles/packages/",  # noqa E501
]


def async_client(workers: int = 8, retries: int = 3) -> httpx.AsyncClient:
    limits = httpx.Limits(
        max_keepalive_connections=workers, max_connections=workers * 2
    )
    timeout = httpx.Timeout(None)
    transport = httpx.AsyncHTTPTransport(retries=retries, limits=limits)
    return httpx.AsyncClient(
        transport=transport, timeout=timeout, follow_redirects=True
    )


async def download_packages(
    base: str,
    lockfile: Path,
    nvras: Mapping[str, PriorityQueue],
    koji_mirrors: Collection[str],
    output_dir: Path,
):
    """Download the set of packages in `lockfile` using `nvras` as a download
    URL lookup.

    If a package in the lockfile cannot be found in the package lookup, a list
    of Koji mirrors is used as a fallback.

    Parameters:
        base: The base distribution for the packages being downloaded.
        lockfile: The path to the lockfile.
        nvras: A mapping from package NVRA to its download URLs in descending
        priority order.
        koji_mirrors: A collection of Koji mirror URLs that will be used in the
                    event a package download URL can't be found in the NVRA
                    lookup.
        output_dir: The parent directory where the YUM repo structure will be
                    created and downloaded RPMs will be written to.
    """
    # Create clean output directory structure
    output_dir.mkdir(parents=True, exist_ok=True)

    with lockfile.open() as f:
        lock = json.load(f)

    if base not in lock:
        raise KeyError(
            "Malformed lockfile: top level key did not match 'cs9' or 'rhel9'."
        )

    cache_path = Path(__file__).parent / "koji_cache.json"
    koji_cache = _load_koji_cache(cache_path)

    client = async_client()
    tasks = []
    common = lock[base]["common"]

    for arch in SUPPORTED_ARCHES:
        repo_path = output_dir / base / arch / "os" / "Packages"

        if repo_path.exists():
            logging.info("Removing existing repo structure at %s", repo_path)
            shutil.rmtree(str(repo_path))

        logging.info("Creating repo structure at %s", repo_path)
        repo_path.mkdir(parents=True)

        packages = common + lock[base]["arch"][arch]
        for package in packages:
            future = asyncio.create_task(
                _download_package(
                    package,
                    arch,
                    repo_path,
                    nvras,
                    koji_mirrors,
                    koji_cache,
                    client,
                )
            )
            tasks.append(future)

    logging.info(
        "Beginning all package downloads (%d tasks in queue)...", len(tasks)
    )
    try:
        # asyncio.gather without `return_exceptions=True` exits on
        # the first Exception it encounters
        await asyncio.gather(*tasks)
    finally:
        await client.aclose()
        _save_koji_cache(koji_cache, cache_path)


async def _download_package(
    package: str,
    package_arch: str,
    output_dir: Path,
    nvras: Mapping[str, PriorityQueue],
    koji_mirrors: Collection[str],
    koji_cache: MutableMapping[str, str],
    client: httpx.AsyncClient,
) -> Path:
    kwargs = {
        "package": package,
        "nvras": nvras,
        "koji_mirrors": koji_mirrors,
        "koji_cache": koji_cache,
        "client": client,
    }
    # in order of preference, try these sources for a package
    for func in [_try_index, _try_koji]:
        # first try it by the arch we know, then fallback to noarch
        for arch in [package_arch, "noarch"]:
            kwargs["arch"] = arch

            rpm_name = f"{package}.{arch}.rpm"
            output_file = output_dir / rpm_name
            kwargs["output_path"] = output_file

            try:
                if await func(**kwargs):
                    return output_file
            except Exception:
                logging.warning(
                    "Failed to find %s.%s trying next arch...", package, arch
                )
                continue
        logging.warning(
            "Failed to find %s with %s. Trying next option...",
            package,
            func.__name__,
        )

    raise RuntimeError(f"Unable to download {package}.")


async def _try_index(
    *,
    package: str = None,
    arch: str = None,
    output_path: Path = None,
    nvras: Mapping[str, PriorityQueue] = None,
    client: httpx.AsyncClient = None,
    **_,
) -> bool:
    if any(
        (arg is None for arg in (package, arch, output_path, nvras, client))
    ):
        raise ValueError(
            (
                "Unable to try compose mirrors due to unfulfilled function "
                "argument."
            )
        )
    if (nvra := f"{package}-{arch}") not in nvras:
        return False
    source = nvras[nvra].get_nowait()
    logging.info("Found %s in package lookup! Downloading %s...", nvra, source)
    await _download_file(
        client, source.uri, _sanitize_name_for_s3(output_path)
    )
    return True


async def _try_koji(
    *,
    package: str = None,
    arch: str = None,
    output_path: Path = None,
    koji_mirrors: list[str] = None,
    koji_cache: MutableMapping[str, str] = None,
    client: httpx.AsyncClient = None,
    **_,
):
    if any(
        (
            arg is None
            for arg in (
                package,
                arch,
                output_path,
                koji_mirrors,
                koji_cache,
                client,
            )
        )
    ):
        raise ValueError(
            "Failed to try Koji mirrors due to unfulfilled function argument"
        )

    for mirror in koji_mirrors:
        api_url, packages_url = mirror.split("|")
        # Have to search koji/brew for the package name
        # eg. libgcc comes from gcc
        session = koji.ClientSession(api_url, opts={"no_ssl_verify": True})
        # Find build of RPM
        logging.info(
            "Trying Koji/Brew (%s) for %s.%s",
            mirror,
            package,
            arch,
        )

        # break apart the package name for all the URL path
        # components needed for Koji
        name, version, release = package.rsplit("-", 2)

        # see if we've ever come across this package before, and
        # can save the API call to find out where it comes from
        if name in koji_cache:
            koji_build_name = koji_cache[name]
        else:
            try:
                koji_build_name = _find_koji_package_name(
                    package, arch, session
                )
            except Exception:
                koji_build_name = None

        if koji_build_name is None:
            return False

        koji_cache[name] = koji_build_name

        url = urljoin(
            packages_url,
            f"{koji_build_name}/{version}/{release}/{arch}/{output_path.name}",
        )

        try:
            await _download_file(
                client, url, _sanitize_name_for_s3(output_path)
            )
            return True
        except Exception:
            logging.error("Error downloading from Koji %s", mirror)
            # ignore exception and try next mirror
            pass

    return False


def _load_koji_cache(path: Path) -> MutableMapping[Any, Any]:
    if not path.exists():
        return {}

    with path.open("r") as f:
        return json.load(f)


def _save_koji_cache(cache: Mapping[Any, Any], path: Path):
    # ensure we serialize an empty object at least
    cache = cache or {}
    with path.open("w") as f:
        j = json.dumps(cache, indent=2) + os.linesep
        f.write(j)


def _find_koji_package_name(
    package: str, arch: str, koji_session: koji.ClientSession
) -> Optional[str]:
    nvra = f"{package}.{arch}"
    logging.info("Checking Koji for build info for %s...", nvra)
    find_build = koji_session.getRPM(nvra)
    if find_build is None:
        return None

    logging.info("Build info: %s", json.dumps(find_build))
    logging.info("Checking Koji for package info for %s...", nvra)
    find_package = koji_session.getBuild(find_build["build_id"])
    if find_package is None:
        return None

    package_name = find_package["name"]

    # Corner case: seemingly no Koji or other API to determine this
    if package_name == "nspr":
        package_name = "nss"

    return package_name


async def _download_file(
    client: httpx.AsyncClient, url: str, output_path: Path
):
    response = await client.get(url)
    response.raise_for_status()

    async with AIOFile(output_path, "wb") as f:
        content = await response.aread()
        await f.write(content)

    await response.aclose()
    # logging.info("Done!")


def _sanitize_name_for_s3(path: Path):
    """Sanitize the '+' character from path names for Amazon S3.

    It is a known issue that S3 doesn't like the character '+' in
    URLs (PATH or file names):
    https://stackoverflow.com/questions/36734171/how-to-decide-if-the-filename-has-a-plus-sign-in-it # noqa E501
    """
    return path.with_name(path.name.replace("+", "-"))
