from pathlib import Path
from urllib.parse import urljoin

import httpx
import pytest
import respx

from create_yum_repo.nvra import (
    _fetch_primary_xml,
    _fetch_repomd_xml,
    _primary_xml_path_from_repomd,
)


@pytest.mark.asyncio
async def test_fetch_repomd_xml(async_client, repomd_xml_path):
    base_url = "http://mirror.stream.centos.org"
    route = "/9-stream/BaseOS/x86_64/os/repodata/repomd.xml"
    url = urljoin(base_url, route)
    with respx.mock(base_url=base_url) as mock_router, repomd_xml_path.open(
        "r"
    ) as content:
        mock_router.get(route).respond(text=content.read())
        root = await _fetch_repomd_xml(url, async_client)

    assert root.tag == "{http://linux.duke.edu/metadata/repo}repomd"


def test_find_primary_xml_path(repomd_xml_tree):
    expected = "repodata/05a02378d6c8959f7011a33bd45f65e1fb438aa4f00327ab782601067cc778e5-primary.xml.gz"  # noqa
    actual = _primary_xml_path_from_repomd(repomd_xml_tree)
    assert expected == actual


@pytest.mark.asyncio
async def test_fetch_primary_xml(
    async_client: httpx.AsyncClient, primary_xml_path: Path
):
    base_url = "http://mirror.stream.centos.org"
    route = "/9-stream/BaseOS/x86_64/os/repodata/05a02378d6c8959f7011a33bd45f65e1fb438aa4f00327ab782601067cc778e5-primary.xml.gz"  # noqa
    url = urljoin(base_url, route)
    with respx.mock(base_url=base_url) as mock_router, primary_xml_path.open(
        "rb"
    ) as content:
        mock_router.get(route).respond(content=content.read())
        root = await _fetch_primary_xml(url, async_client)

    assert root.tag == "{http://linux.duke.edu/metadata/common}metadata"
