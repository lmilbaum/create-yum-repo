import sys
from unittest.mock import call, patch

import pytest

from create_yum_repo import sanity_test
from create_yum_repo.sanity_test import verify_repo


@pytest.mark.asyncio
async def test_verify_repo():
    with patch(
        "create_yum_repo.sanity_test.dnf_makecache"
    ) as mock_dnf_makecache:
        await verify_repo("file://fake.repo", "x86_64")
    mock_dnf_makecache.assert_called()


def test_entrypoint():
    repo = "https://fake-repo"
    with patch.object(sys, "argv", ["repo-sanity-test", repo]), patch(
        "create_yum_repo.sanity_test.verify_repo"
    ) as mock_verify_repo:
        sanity_test.run()

    mock_verify_repo.assert_has_calls(
        [call(repo, arch) for arch in ("aarch64", "x86_64")]
    )
