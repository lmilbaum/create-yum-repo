import hashlib
import json
from pathlib import Path
from typing import BinaryIO
from unittest.mock import MagicMock, patch

import httpx
import pytest
import respx
from httpx import HTTPError

from create_yum_repo import download
from create_yum_repo.download import (
    DEFAULT_KOJI_INSTANCES,
    SUPPORTED_ARCHES,
    _download_file,
    _download_package,
    _find_koji_package_name,
    _load_koji_cache,
    _save_koji_cache,
    _try_index,
    _try_koji,
    download_packages,
)

UNIT_TEST_DIRECTORY = Path(__file__).parent
RESOURCES = UNIT_TEST_DIRECTORY.parent / "resources"


@pytest.mark.asyncio
async def test_download_package_tries_all_sources_when_one_function_raises(
    mock_nvra_lookup, tmp_path, async_client, mock_load_koji_cache
):
    package = "glibc-2.34-39.el9"
    arch = "x86_64"

    mock_koji_cache = mock_load_koji_cache()

    with patch(
        "create_yum_repo.download._try_index", create=True
    ) as mock_try_index, patch(
        "create_yum_repo.download._try_koji", create=True
    ) as mock_try_koji:
        mock_try_koji.__name__ = "_try_koji"
        mock_try_koji.return_value = True

        mock_try_index.__name__ = "_try_index"
        mock_try_index.side_effect = RuntimeError(
            "This is a testing side effect."
        )

        result = await _download_package(
            package,
            arch,
            tmp_path,
            mock_nvra_lookup,
            DEFAULT_KOJI_INSTANCES,
            mock_koji_cache,
            async_client,
        )

    # make sure we tried the package lookup for both `arch` and "noarch"
    assert mock_try_index.call_count == 2

    mock_try_koji.assert_called_once()

    assert result == (tmp_path / f"{package}.{arch}.rpm")


@pytest.mark.asyncio
async def test_download_package_tries_all_sources_and_fails(
    mock_nvra_lookup, tmp_path, async_client, mock_load_koji_cache
):
    package = "glibc-2.34-39.el9"
    arch = "x86_64"

    mock_koji_cache = mock_load_koji_cache()

    with patch(
        "create_yum_repo.download._try_index", create=True
    ) as mock_try_index, patch(
        "create_yum_repo.download._try_koji", create=True
    ) as mock_try_koji:
        mock_try_index.__name__ = "_try_index"
        mock_try_index.return_value = False

        mock_try_koji.__name__ = "_try_koji"
        mock_try_koji.return_value = False

        with pytest.raises(RuntimeError):
            await _download_package(
                package,
                arch,
                tmp_path,
                mock_nvra_lookup,
                DEFAULT_KOJI_INSTANCES,
                mock_koji_cache,
                async_client,
            )

    # make sure we tried with both `arch` and "noarch"
    assert mock_try_index.call_count == 2
    assert mock_try_koji.call_count == 2


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "package,expected",
    [("glibc-devel-2.34-39.el9", False), ("glibc-2.34-39.el9", True)],
)
async def test_try_index(
    package, expected, tmp_path, mock_nvra_lookup, async_client
):
    arch = "x86_64"
    output_path = tmp_path / "test.rpm"

    with patch("create_yum_repo.download._download_file") as mock_download:
        result = await _try_index(
            package=package,
            arch=arch,
            output_path=output_path,
            nvras=mock_nvra_lookup,
            client=async_client,
        )
        assert result is expected

    if result:
        mock_download.assert_called_once_with(
            async_client,
            "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/BaseOS/x86_64/os/Packages/glibc-2.34-39.el9.x86_64.rpm",  # noqa E501
            output_path,
        )
    else:
        mock_download.assert_not_called()


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "package,arch,output_path,nvras,client",
    [
        (None, "x86_64", Path("/tmp/test.txt"), {}, respx.mock()),
        ("glibc-2.34-39.el9", None, Path("/tmp/test.txt"), {}, respx.mock()),
        ("glibc-2.34-39.el9", "x86_64", None, {}, respx.mock()),
        (
            "glibc-2.34-39.el9",
            "x86_64",
            Path("/tmp/test.txt"),
            None,
            respx.mock(),
        ),
        ("glibc-2.34-39.el9", "x86_64", Path("/tmp/test.txt"), {}, None),
    ],
)
async def test_try_index_raises_on_missing_argument(
    package, arch, output_path, nvras, client
):
    kwargs = {
        "package": package,
        "arch": arch,
        "output_path": output_path,
        "nvras": nvras,
        "client": client,
    }
    with pytest.raises(ValueError):
        await _try_index(**kwargs)


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "package,requires_koji_calls,koji_name",
    [
        ("glibc-devel-2.34-39.el9", True, "glibc"),
        ("glibc-2.34-39.el9", False, "glibc"),
    ],
)
async def test_try_koji_success(
    package,
    requires_koji_calls,
    koji_name,
    mock_koji_cache,
    tmp_path,
    async_client,
):
    arch = "x86_64"
    output_path = tmp_path / "test.rpm"

    with patch(
        "create_yum_repo.download._download_file"
    ) as mock_download, patch(
        "create_yum_repo.download._find_koji_package_name"
    ) as mock_find_koji_name:

        if requires_koji_calls:
            mock_find_koji_name.return_value = koji_name

        result = await _try_koji(
            package=package,
            arch=arch,
            output_path=output_path,
            koji_mirrors=[
                "https://kojihub.stream.centos.org/kojihub/|https://kojihub.stream.centos.org/kojifiles/packages/"  # noqa E501
            ],
            koji_cache=mock_koji_cache,
            client=async_client,
        )
        assert result

    _, v, r = package.rsplit("-", 2)
    mock_download.assert_called_once_with(
        async_client,
        (
            "https://kojihub.stream.centos.org/kojifiles/packages/"
            f"{koji_name}/{v}/{r}/{arch}/{output_path.name}"
        ),
        output_path,
    )

    if requires_koji_calls:
        mock_find_koji_name.assert_called()
    else:
        mock_find_koji_name.assert_not_called()


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "package,requires_koji_calls,koji_name",
    [
        ("glibc-devel-2.34-39.el9", True, "glibc"),
        ("glibc-2.34-39.el9", False, "glibc"),
    ],
)
async def test_try_koji_fails_exhausts_mirrors(
    package,
    requires_koji_calls,
    koji_name,
    mock_koji_cache,
    tmp_path,
    async_client,
):
    arch = "x86_64"
    output_path = tmp_path / "test.rpm"

    with patch(
        "create_yum_repo.download._download_file"
    ) as mock_download, patch(
        "create_yum_repo.download._find_koji_package_name"
    ) as mock_find_koji_name:

        mock_download.side_effect = HTTPError(
            "Fake HTTP error for use during testing."
        )

        if requires_koji_calls:
            mock_find_koji_name.return_value = koji_name

        result = await _try_koji(
            package=package,
            arch=arch,
            output_path=output_path,
            koji_mirrors=DEFAULT_KOJI_INSTANCES,
            koji_cache=mock_koji_cache,
            client=async_client,
        )

        assert not result

    assert mock_download.call_count == len(DEFAULT_KOJI_INSTANCES)

    if requires_koji_calls:
        mock_find_koji_name.assert_called()
    else:
        mock_find_koji_name.assert_not_called()


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "package,arch,output_path,koji_mirrors,koji_cache,client",
    [
        (
            None,
            "x86_64",
            Path("/tmp/test.txt"),
            {},
            {"glibc": "glibc"},
            respx.mock(),
        ),
        (
            "glibc-2.34-39.el9",
            None,
            Path("/tmp/test.txt"),
            {},
            {"glibc": "glibc"},
            respx.mock(),
        ),
        (
            "glibc-2.34-39.el9",
            "x86_64",
            None,
            {},
            {"glibc": "glibc"},
            respx.mock(),
        ),
        (
            "glibc-2.34-39.el9",
            "x86_64",
            Path("/tmp/test.txt"),
            None,
            {"glibc": "glibc"},
            respx.mock(),
        ),
        (
            "glibc-2.34-39.el9",
            "x86_64",
            Path("/tmp/test.txt"),
            {},
            None,
            respx.mock(),
        ),
        (
            "glibc-2.34-39.el9",
            "x86_64",
            Path("/tmp/test.txt"),
            {},
            {"glibc": "glibc"},
            None,
        ),
    ],
)
async def test_try_koji_raises_on_missing_argument(
    package, arch, output_path, koji_mirrors, koji_cache, client
):
    kwargs = {
        "package": package,
        "arch": arch,
        "output_path": output_path,
        "koji_mirrors": koji_mirrors,
        "koji_cache": koji_cache,
        "client": client,
    }
    with pytest.raises(ValueError):
        await _try_koji(**kwargs)


@pytest.mark.asyncio
async def test_try_koji_does_not_download_when_no_koji_name_is_found(
    mock_koji_cache,
    tmp_path,
    async_client,
):
    arch = "x86_64"
    output_path = tmp_path / "test.rpm"
    package = "some-random-package-0.1.0-el9"

    with patch(
        "create_yum_repo.download._download_file"
    ) as mock_download, patch(
        "create_yum_repo.download._find_koji_package_name"
    ) as mock_find_koji_name:

        mock_find_koji_name.return_value = None

        result = await _try_koji(
            package=package,
            arch=arch,
            output_path=output_path,
            koji_mirrors=[
                "https://kojihub.stream.centos.org/kojihub/|https://kojihub.stream.centos.org/kojifiles/packages/"  # noqa E501
            ],
            koji_cache=mock_koji_cache,
            client=async_client,
        )

        assert not result

    mock_find_koji_name.assert_called()
    mock_download.assert_not_called()


@pytest.mark.parametrize(
    "path,exists",
    [
        (RESOURCES / "fake_koji_cache.json", True),
        (RESOURCES / "doesnt_exist.json", False),
    ],
)
def test_load_koji_cache(path, exists):
    expected = {"glibc": "glibc", "glibc-devel": "glibc"} if exists else {}
    actual = _load_koji_cache(path)
    assert actual == expected


@pytest.mark.parametrize(
    "cache", [{}, None, {"glibc": "glibc", "glibc-devel": "glibc"}]
)
def test_save_koji_cache(cache, tmp_path):
    actual = tmp_path / "test_koji_cache.json"

    if cache:
        expected = RESOURCES / "fake_koji_cache.json"
    else:
        expected = RESOURCES / "fake_koji_cache_empty.json"

    _save_koji_cache(cache, actual)
    assert_files(expected, actual)


@pytest.mark.parametrize(
    "package,rpm_response,build_response,expected",
    [
        ("glibc-devel-2.34-39.el9", "resources", "resources", "glibc"),
        ("glibc-2.34-39.el9", "resources", None, None),
        ("wont-find-an-rpm", None, None, None),
        (  # exercise a corner case
            "nspr-devel-4.34.0-7.el9",
            "resources",
            {"name": "nspr"},
            "nss",
        ),
    ],
)
def test_find_koji_package_name(
    package, rpm_response, build_response, expected
):
    arch = "x86_64"
    koji = MagicMock(getRPM=MagicMock(), getBuild=MagicMock())

    if rpm_response == "resources":
        with (RESOURCES / "getRPM.json").open("r") as f:
            koji.getRPM.return_value = json.load(f)
    else:
        koji.getRPM.return_value = rpm_response

    if build_response == "resources":
        with (RESOURCES / "getBuild.json").open("r") as f:
            koji.getBuild.return_value = json.load(f)
    else:
        koji.getBuild.return_value = build_response

    actual = _find_koji_package_name(package, arch, koji)

    assert actual == expected


@pytest.mark.asyncio
async def test_download_file(
    async_client, respx_mock, repomd_xml_path, tmp_path
):
    url = "http://example.com"
    route = respx_mock.get(url)
    output_path = tmp_path / "fake.xml"

    with repomd_xml_path.open("rb") as content:
        route.respond(content=content.read())
        await _download_file(async_client, url, output_path)

    assert_files(repomd_xml_path, output_path)


def assert_files(expected: Path, actual: Path):
    with expected.open("rb") as e, actual.open("rb") as a:
        assert _hash(e) == _hash(a)


def _hash(file: BinaryIO) -> str:
    digester = hashlib.sha256()
    while chunk := file.read(4096):
        digester.update(chunk)

    return digester.hexdigest()


@pytest.mark.asyncio
async def test_download_packages(
    mock_nvra_lookup,
    async_client,
    tmp_path,
    mock_load_koji_cache,
    mock_save_koji_cache,
):
    lockfile = RESOURCES / "fake_lockfile.lock.json"

    # create one existing repo structure to exercise a particular branch
    (tmp_path / "cs9/x86_64/os/Packages").mkdir(parents=True)

    with lockfile.open() as f:
        lock = json.load(f)

    # duplicate each common package once per supported arch
    packages = lock["cs9"]["common"] * len(SUPPORTED_ARCHES)
    # then add the arch-specific packages
    for key in SUPPORTED_ARCHES:
        packages.extend(lock["cs9"]["arch"][key])

    with patch(
        "create_yum_repo.download.async_client"
    ) as mock_http_factory, patch(
        "create_yum_repo.download._download_package"
    ) as mock_download_package:
        mock_http_factory.return_value = async_client
        await download_packages(
            "cs9", lockfile, mock_nvra_lookup, DEFAULT_KOJI_INSTANCES, tmp_path
        )

    assert mock_download_package.call_count == len(packages)
    assert async_client.is_closed


@pytest.mark.asyncio
async def test_download_packages_raises_on_malformed_lockfile(
    mock_nvra_lookup, tmp_path
):
    lockfile = RESOURCES / "bad_lockfile.lock.json"
    with pytest.raises(KeyError):
        await download_packages(
            "cs9", lockfile, mock_nvra_lookup, DEFAULT_KOJI_INSTANCES, tmp_path
        )


def test_async_client() -> bool:
    assert type(download.async_client()) == httpx.AsyncClient
