import pytest

from create_yum_repo.nvra import ARBITRARILY_LOW_PRIORITY
from create_yum_repo.rpm_source import RpmSource

_RAISE = object()


@pytest.mark.parametrize(
    "source,other,operator",
    [
        (("file://fake/uri", 1), ("file://fake/uri", 1), "__eq__"),
        (("file://fake/uri", 1), ("file://fake/uri", 0), "__ne__"),
        (
            ("file://fake/uri", 1),
            ("file://fake/uri", ARBITRARILY_LOW_PRIORITY),
            "__lt__",
        ),
        (("file://fake/uri", 1), ("file://fake/uri", 1), "__le__"),
        (
            ("file://fake/uri", ARBITRARILY_LOW_PRIORITY),
            ("file://fake/uri", 1),
            "__gt__",
        ),
        (("file://fake/uri", 1), ("file://fake/uri", 1), "__ge__"),
        (("file://fake/uri", 1), ("file://fake/uri", 1), "equals"),
        (("file://fake/uri", 1), (), _RAISE),
    ],
)
def test_rpm_source_comparisons(source, other, operator):
    rpm_source1 = RpmSource(*source)

    if operator is _RAISE:
        with pytest.raises(TypeError) as exc_info:
            # compare an RpmSource to some other type
            # noinspection PyStatementEffect
            rpm_source1 == other
        error = exc_info.value
        assert "Cannot compare RpmSource and <class 'tuple'> with ==" == str(
            error
        )
    else:
        rpm_source2 = RpmSource(*other)
        func = getattr(rpm_source1, operator)
        assert func(rpm_source2)
