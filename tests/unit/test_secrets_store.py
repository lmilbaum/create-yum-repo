import secrets_store


def test_secrets_store_package_installed() -> bool:
    assert "get_secret_value" in dir(secrets_store)
