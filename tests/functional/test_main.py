import shutil

import pygit2
import pytest

from create_yum_repo import main


def test_main(capsys) -> bool:

    WORKSPACE = "/tmp/create_yum_repo"

    try:
        pygit2.clone_repository(
            url="https://gitlab.com/CentOS/automotive/sample-images.git",
            path=WORKSPACE,
            checkout_branch="main",
        )

        main.main(
            [
                "cs9",
                "-l",
                WORKSPACE + "/package_list/cs9-image-manifest.lock.json",
                "-o",
                WORKSPACE,
            ],
        )
        out, err = capsys.readouterr()
        assert out == ""
        assert err == ""
    except Exception as e:
        pytest.fail(reason=str(e), pytrace=True)
    finally:
        shutil.rmtree(WORKSPACE)
