import sys
from pathlib import Path
from unittest.mock import call, patch

import pytest

from create_yum_repo.download import DEFAULT_KOJI_INSTANCES, SUPPORTED_ARCHES
from create_yum_repo.main import run


@pytest.mark.asyncio
@patch.object(
    sys,
    "argv",
    [
        "create-yum-repo",
        "cs9",
        "-l",
        "../resources/fake_lockfile.lock.json",
        "-o",
        "/tmp/test_create_yum_repo",
        "-r",
        "http://mirror.stream.centos.org/9-stream/{centos_repo}/{arch}/os/",
        "-r",
        "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/{centos_repo}/{arch}/os/",  # noqa E501
    ],
)
@patch("create_yum_repo.main.build_nvra_lookup")
async def test_run(
    mock_build_nvra_lookup,
    mock_nvra_lookup,
):
    expected_base_distro = "cs9"
    expected_lockfile = Path("../resources/fake_lockfile.lock.json")
    expected_output_dir = Path("/tmp/test_create_yum_repo")
    expected_repo_priorities = {
        "mirror.stream.centos.org": 0,
        "composes.stream.centos.org": 1,
    }

    expected_repo_urls = {
        "http://mirror.stream.centos.org/9-stream/BaseOS/x86_64/os/",
        "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/BaseOS/x86_64/os/",  # noqa E501
        "http://mirror.stream.centos.org/9-stream/AppStream/x86_64/os/",
        "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/AppStream/x86_64/os/",  # noqa E501
        "http://mirror.stream.centos.org/9-stream/CRB/x86_64/os/",
        "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/CRB/x86_64/os/",  # noqa E501
        "http://mirror.stream.centos.org/9-stream/BaseOS/aarch64/os/",
        "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/BaseOS/aarch64/os/",  # noqa E501
        "http://mirror.stream.centos.org/9-stream/AppStream/aarch64/os/",
        "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/AppStream/aarch64/os/",  # noqa E501
        "http://mirror.stream.centos.org/9-stream/CRB/aarch64/os/",
        "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/CRB/aarch64/os/",  # noqa E501
    }

    mock_build_nvra_lookup.return_value = mock_nvra_lookup

    with patch("create_yum_repo.main.create_repo") as mock_create_repo, patch(
        "create_yum_repo.main.create_index_html"
    ) as mock_create_index, patch(
        "create_yum_repo.main.download_packages"
    ) as mock_download_packages:
        await run()

    mock_build_nvra_lookup.assert_called_once_with(
        expected_repo_urls, expected_repo_priorities
    )
    mock_download_packages.assert_called_once_with(
        expected_base_distro,
        expected_lockfile,
        mock_nvra_lookup,
        DEFAULT_KOJI_INSTANCES,
        expected_output_dir,
    )

    expected_create_repo_calls = [
        call(expected_output_dir / expected_base_distro / arch / "os")
        for arch in SUPPORTED_ARCHES
    ]
    mock_create_repo.assert_has_calls(
        expected_create_repo_calls, any_order=True
    )
    mock_create_index.assert_called_once_with(expected_output_dir)
