from asyncio import PriorityQueue
from unittest.mock import patch
from urllib.parse import urljoin, urlparse

import pytest
import respx

from create_yum_repo.nvra import ARBITRARILY_LOW_PRIORITY, build_nvra_lookup
from create_yum_repo.rpm_source import RpmSource


@respx.mock
@pytest.mark.asyncio
@patch("create_yum_repo.download.async_client")
async def test_build_nvra_lookup(
    mock_client_factory, async_client, repomd_xml_path, primary_xml_path
):
    mock_client_factory.return_value = async_client

    repos = [
        "http://mirror.stream.centos.org/9-stream/BaseOS/aarch64/os/",
        "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/BaseOS/aarch64/os/",  # noqa E501
        "https://missing.not.real.repository.centos.org/latest/development/nightly/BaseOS/aarch64/os/",  # noqa E501
    ]

    # intentionally leave out missing.not.real.repository.centos.org so that
    # nvra uses the default value of ARBITRARILY_LOW_PRIORITY
    priorities = {
        "mirror.stream.centos.org": 0,
        "composes.stream.centos.org": 1,
    }

    nvra = "glibc-devel-2.34-32.el9-aarch64"
    expected_nvras = {nvra: PriorityQueue()}
    for repo in repos:
        host = urlparse(repo).netloc
        prio = priorities.get(host, ARBITRARILY_LOW_PRIORITY)
        await expected_nvras[nvra].put(
            RpmSource(
                priority=prio,
                uri=urljoin(repo, f"Packages/{nvra}.rpm"),
            )
        )

    with repomd_xml_path.open("r") as fake_repomd, primary_xml_path.open(
        "rb"
    ) as fake_primary:
        # must be a tuple because respx host__in is expecting a hashable type
        hosts = tuple(urlparse(repo).netloc for repo in repos)

        respx.route(
            host__in=hosts, path__regex=".*/repodata/repomd.xml$"
        ).respond(text=fake_repomd.read())

        respx.route(host__in=hosts, path__regex=".*primary.xml.gz$").respond(
            content=fake_primary.read()
        )

        actual_nvras = await build_nvra_lookup(repos, priorities)

    assert len(actual_nvras) == len(expected_nvras)
    assert actual_nvras[nvra].qsize() == expected_nvras[nvra].qsize()
    assert (
        actual_nvras[nvra]
        .get_nowait()
        .equals(
            RpmSource(
                priority=0,
                uri="http://mirror.stream.centos.org/9-stream/BaseOS/aarch64/os/Packages/glibc-devel-2.34-32.el9.aarch64.rpm",  # noqa E501
            )
        )
    )
