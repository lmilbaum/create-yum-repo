from asyncio import PriorityQueue
from pathlib import Path
from unittest.mock import patch
from xml.etree.ElementTree import Element, fromstring

import httpx
import pytest

from create_yum_repo import download
from create_yum_repo.rpm_source import RpmSource

UNIT_TEST_DIRECTORY = Path(__file__).parent
RESOURCES = UNIT_TEST_DIRECTORY / "resources"


@pytest.fixture
async def async_client() -> httpx.AsyncClient:
    async with download.async_client() as client:
        yield client
    assert (
        client.is_closed
    ), "ERROR: async_client fixture did not auto-close the AsyncClient."


@pytest.fixture
def repomd_xml_path() -> Path:
    return RESOURCES / "repomd.xml"


@pytest.fixture
def repomd_xml(repomd_xml_path: Path) -> str:
    with repomd_xml_path.open("r") as xml_file:
        return xml_file.read()


@pytest.fixture
def repomd_xml_tree(repomd_xml: str) -> Element:
    return fromstring(repomd_xml)


@pytest.fixture
def primary_xml_path() -> Path:
    return RESOURCES / "fake_primary.xml.gz"


@pytest.fixture
def mock_load_koji_cache():
    with patch("create_yum_repo.download._load_koji_cache") as mock_load_cache:
        mock_load_cache.return_value = {"glibc": "glibc"}
        yield mock_load_cache
    mock_load_cache.assert_called()


@pytest.fixture
def mock_save_koji_cache():
    with patch("create_yum_repo.download._save_koji_cache") as mock_save_cache:
        yield mock_save_cache
    mock_save_cache.assert_called()


@pytest.fixture
def mock_koji_cache(mock_load_koji_cache):
    yield mock_load_koji_cache()


@pytest.fixture
def mock_nvra_lookup():
    rpm_source = RpmSource(
        (
            "https://composes.stream.centos.org/development/"
            "latest-CentOS-Stream/compose/BaseOS/x86_64/os/Packages/"
            "glibc-2.34-39.el9.x86_64.rpm"
        ),
        priority=0,
    )
    queue = PriorityQueue()
    queue.put_nowait(rpm_source)
    return {"glibc-2.34-39.el9-x86_64": queue}
