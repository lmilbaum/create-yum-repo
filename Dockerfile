FROM registry.access.redhat.com/ubi8/python-39:1-87.1669838026 as builder
USER root
ENV POETRY_HOME /opt/poetry
ENV PATH "${POETRY_HOME}/bin:${PATH}"
WORKDIR /tmp/build
COPY . .
RUN make depend && poetry build

FROM quay.io/centos/centos:stream8
USER root
WORKDIR /opt/create_yum_repo
COPY --from=builder /tmp/build/dist/*.whl .
RUN dnf install -y createrepo_c-0.17.7-6.el8 \
    krb5-devel-1.18.2-21.el8 \
    python39-pip-20.2.4-7.module_el8.6.0+961+ca697fb5 \
    python39-devel-3.9.13-1.module_el8.7.0+1178+0ba51308 \
    tree-1.7.0-15.el8 \
    diffutils-3.6-6.el8 \
    git-2.31.1-2.el8 \
    gcc-8.5.0-15.el8 \
    make-1:4.2.1-11.el8.x86_64 \
    && dnf clean all
RUN pip3 install --no-cache-dir \
    --index-url https://gitlab.com/api/v4/projects/37904501/packages/pypi/simple secrets_store==0.0.1 \
    ./*.whl && rm -rf ./*.whl
