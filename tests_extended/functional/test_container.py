import subprocess


def test_container(container_engine, create_testing_image):
    command = (
        f"{container_engine} run {create_testing_image} create-yum-repo "
        "cs9 -l "
        "/tmp/create_yum_repo/sample-images/package_list/"
        "cs9-image-manifest.lock.json"
    )

    completedProcess = subprocess.run(
        command.split(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        check=True,
    )

    assert completedProcess.returncode == 0
