import os
import shutil
import subprocess

import pytest


@pytest.fixture(scope="session")
def container_engine() -> str:
    if shutil.which("podman"):
        return "podman"
    else:
        return "docker"


@pytest.fixture(scope="session")
def create_testing_image(container_engine) -> str:
    image_name = "image_under_test"
    if "CI" not in os.environ:
        build_args = (
            "--build-arg IMAGE_UNDER_TEST_NAME=localhost/create_yum_repo"
        )
    else:
        build_args = ""
    command = (
        f"{container_engine} build "
        f"-f {os.getcwd()}/tests_extended/functional/resources/Dockerfile "
        f"{build_args} -t {image_name} ."
    )

    subprocess.run(
        command.split(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        check=True,
    )
    return image_name
