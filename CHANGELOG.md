# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [Unreleased]

### Added

* Roetry
  ([!107](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/107))
* Renovate presets
  ([!100](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/100))
* Refactor the how httpx async client timeout is configured
  ([!91](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/91))
* Consolidate lint into precommit
  ([!55](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/55))
* Optimize testing Dockerfile
  ([!57](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/57))
* s/Containerfile/Dockerfile/ because there are tools which do not work with Containerfile
  ([!56](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/56))
* A different way to consume python packages which complies with Renovate
  ([!54](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/54))
* Automated Dependency Updates for Pre Commit with Renovate and Validate
  renovate.json file with pre-commit
  ([!45](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/45))
* Fixing markdownlint reported issues
  ([!43](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/43))
* Fixing trailing-whitespace reported issues
  ([!42](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/42))
* Fixing black reported issues
  ([!41](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/41))
* Automated Dependency Updates for Pre Commit with Renovate and Validate
  renovate.json file with pre-commit
  ([!33](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/33))
* precommit job added to CI. This change break the CI because the precommit
  quality gate is broken due to previous issues. Those will be fixed in the
  following MRs.
  ([!36](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/36))
* changelog
  ([!35](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/35))
* secrets store: install package
  ([!34](https://gitlab.com/CentOS/automotive/coffre/create-yum-repo/-/merge_requests/34))
