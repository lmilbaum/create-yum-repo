.PHONY: all depend precommit unit functional functional_extended build login \
		publish

ifndef CI
  CONTAINER_ENGINE := podman
  IMAGE_NAME := create_yum_repo
  IMAGE_TAG := latest
  RUN_IN_CONTAINER := ${CONTAINER_ENGINE} run --rm -i \
    -v ${PWD}/.hadolint.yaml:/root/.config/hadolint.yaml:Z \
    -v ${PWD}/Containerfile:/Containerfile:Z \
    ghcr.io/hadolint/hadolint:v2.10.0-debian
endif

ifeq "$(ENV)" "CI"
  CONTAINER_ENGINE := docker
  IMAGE_NAME := ${CI_REGISTRY_IMAGE}
  IMAGE_TAG := ${CI_COMMIT_REF_SLUG}
endif

ifeq "$(ENV)" "PROD"
  CONTAINER_ENGINE := docker
  IMAGE_NAME := ${CI_REGISTRY_IMAGE}
  IMAGE_TAG := latest
endif

all: depend precommit unit build functional functional_extended

depend:
	curl -sSL https://install.python-poetry.org | python3 - --version 1.2.2
	poetry install --with "$a"

precommit:
	poetry run pre-commit run --all-files

unit:
	poetry run pytest --cov=create_yum_repo --cov-report=html --cov-report=xml \
	--cov-report=term-missing --cov-branch --junitxml=junit.xml \
	--cov-fail-under=82 --asyncio-mode=auto -v tests/unit

functional:
	poetry run pytest -vv tests/functional

functional_extended:
	poetry run pytest -vv tests_extended/functional

build:
	${CONTAINER_ENGINE} build -t ${IMAGE_NAME}:${IMAGE_TAG} .

login:
	${CONTAINER_ENGINE} login -u ${CI_REGISTRY_USER} \
	-p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}

publish: build login
	${CONTAINER_ENGINE} push ${IMAGE_NAME}:${IMAGE_TAG}
